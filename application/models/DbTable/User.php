<?php

/*
 * Basic Lookup Table Model
 */

class Model_DbTable_User extends Zend_Db_Table_Abstract
{

    protected $_name = 'users';
    //protected $_sequence = 'entity_seq';
    protected $_primary = 'id';

}