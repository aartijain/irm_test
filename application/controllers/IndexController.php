<?php

class IndexController extends Zend_Controller_Action
{
    protected $flashMessanger = null;
    public function init()
    {
        /* Initialize action controller here */
        $this->flashMessenger = $this->_helper->getHelper('FlashMessenger');
        
    }

    public function indexAction()
    {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->redirect('login');
        }
        $this->view->messages = $this->flashMessenger->getMessages();
    }

    
}

