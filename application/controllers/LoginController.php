<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class LoginController extends Zend_Controller_Action
{
    protected $flashMessenger = null;
    
    public function init()
    {
        /* Initialize action controller here */
        $this->flashMessenger = $this->_helper->getHelper('FlashMessenger');
        //$this->initView();
        $this->hashPasswordOptions = [
            'cost' => 11,
            'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
        ];
    }
   
    public function indexAction()
    {
        if (!Zend_Auth::getInstance()->hasIdentity())
        {
            $form = new Form_Login(array(
                //'action' => '/login/process',
                'method' => 'post',
                'id' => 'irmForm'
            ));   
            
            $request = $this->getRequest();
  
            if ($request->isPost()) {

                $postData = $request->getParams();

                if ($form->isValid($postData)) {
                    $adapter = $this->getAuthAdapter($form->getValues());

                    $auth    = Zend_Auth::getInstance();

                    $result  = $auth->authenticate($adapter);

                    //echo "<pre>"; print_r($result); die; 

                    if ($result->isValid()) {
                        $userTable = new Model_DbTable_User();

                        $userInfo = $adapter->getResultRowObject(
                                array('id', 'username', 'companyname', 'password', 'status')
                                );

                        $where = $userTable->getAdapter()->quoteInto('id = ?', $userInfo->id);
                        $data  = array('password' => $this->generateNewPassword($postData['password']), 'company'=>$postData['companyname']);
                        $result = $userTable->update($data, $where);
                        $this->flashMessenger->addMessage('record updated.');
                    } else {
                        $form->setDescription('Invalid credentials provided');
                        $this->flashMessenger->addMessage("Wrong credentials");
                    }
                } else {
                    $this->flashMessenger->addMessage("Form invalid");
                }

            }
        } else {
            $this->redirect('index', 'index');
        } 
        $this->view->form = $form;
    }

    public function processloginAction()
    {   
        if (Zend_Auth::getInstance()->hasIdentity()) {
            Zend_Auth::getInstance()->clearIdentity();
        }

            $form = new Form_Login(array(
                //'action' => '/login/process',
                'method' => 'post',
                'id' => 'irmForm'
            ));   
            
            $request = $this->getRequest();
  
            if ($request->isPost()) {

                $postData = $request->getParams();

                if ($form->isValid($postData)) {
                    $hashResult = $this->authenticateUser($form->getValues());

                    //echo "<pre>"; print_r($result); die; 
                    if ($hashResult->getCode() == Zend_Auth_Result::SUCCESS) {
                                           // Invalid credentials
                        $auth = Zend_Auth::getInstance();
                        $adapter = new Zend_Auth_Adapter_DbTable();
                        $userTable = new Model_DbTable_User();
                        $postData = $form->getValues();
                        $user = $userTable->fetchRow($userTable->select()
                                ->where('username = ?', $postData['username'])
                                ->where('company = ?', $postData['companyname']));

                        $adapter->setTableName('users')
                                    ->setIdentityColumn(new Zend_Db_Expr('lower(username)'))
                                    ->setCredentialColumn('password')
                                    ->setIdentity($user->username)
                                    ->setCredential($user->password);

                        $auth->authenticate($adapter);
                        return $this->_helper->redirector('index');
                    } else {

                        $this->flashMessenger->addMessage('Invalid credentials provided.');
                        $form->setDescription('Invalid credentials provided');
                        $this->view->form = $form;
                        return $this->render('index'); // re-render the login form
                    }
                } else {
                    $this->flashMessenger->addMessage("Form invalid");
                }

            }
        
        $this->view->form = $form;
    }
    
    public function getAuthAdapter(array $params)
    {      
        $adapter = new Zend_Auth_Adapter_DbTable();
        $adapter->setTableName('users')
                ->setIdentityColumn(new Zend_Db_Expr('lower(username)'))
                ->setCredentialColumn('password')
                ->setIdentity($params['username'])
                //->setCredential($hashPassword);
                //->setCredential($params['password']);
                ->setCredential(strtoupper(sha1(md5($params['password']) . $params['password'])));
       
        return $adapter;
    }
    
    
    public function preDispatch()
    {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            // If the user is logged in, we don't want to show the login form;
            // however, the logout action should still be available
            if ('logout' != $this->getRequest()->getActionName()) {
                $this->_helper->redirector('index', 'index');
            }
        } else {
            // If they aren't, they can't logout, so that action should 
            // redirect to the login form
            if ('logout' == $this->getRequest()->getActionName()) {
                $this->_helper->redirector('index');
            }
        }
    }
    
    
    
    public function processhashloginAction() 
    {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            Zend_Auth::getInstance()->clearIdentity();
        } 
        $this->view->messages = $this->flashMessenger->getMessages();
        $this->render();
        $request = $this->getRequest();
        
        // Check if we have a POST request
        if (!$request->isPost()) {
            
            return $this->_helper->redirector('processlogin');
        }
        
        // Get our form and validate it
        $form = new Form_Login(array(
            //'action' => '/login/processlogin',
            'method' => 'post',
            'id' => 'irmForm'
        ));;
        $form->setAttrib('id', 'irmForm');
        
        
        if (!$form->isValid($request->getPost())) {
            // Invalid entries
            $form->setDescription('Invalid credentials');
            //return $this->_helper->redirector('processlogin'); // re-render the login form
        }
        
        $hashResult = $this->authenticateUser($form->getValues());
        //echo "<pre>Identity "; print_r($hashResult); die;
      
        //echo "<br> Code ".$hashResult->getCode();
        
        
        if ($hashResult->getCode() == Zend_Auth_Result::SUCCESS) {
                       // Invalid credentials
            $auth = Zend_Auth::getInstance();
            $adapter = new Zend_Auth_Adapter_DbTable();
            $userTable = new Model_DbTable_User();
            $postData = $form->getValues();
            $user = $userTable->fetchRow($userTable->select()
                    ->where('username = ?', $postData['username'])
                    ->where('company = ?', $postData['companyname']));

            $adapter->setTableName('users')
                        ->setIdentityColumn(new Zend_Db_Expr('lower(username)'))
                        ->setCredentialColumn('password')
                        ->setIdentity($user->username)
                        ->setCredential($user->password);
                
            $auth->authenticate($adapter);
            return $this->_helper->redirector('index');
        } else {
            
            $this->flashMessenger->addMessage('Invalid credentials provided.');
            $form->setDescription('Invalid credentials provided');
            $this->view->form = $form;
            return $this->render('index'); // re-render the login form
        }
        //$this->view->form = $form;
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
    }
    
    public function authenticateUser(array $params) {

        $adapter = new Zend_Auth_Adapter_DbTable();
        $userTable = new Model_DbTable_User();
        
        $user = $userTable->fetchRow($userTable->select()
                ->where('username = ?', $params['username'])
                ->where('company = ?', $params['companyname']));

        //echo "<pre>"; print_r($user); die;
        if ($user && $user->password) {
            if (password_verify($params['password'], $user->password)) {
                $result = new Zend_Auth_Result(Zend_Auth_Result::SUCCESS, $user);
                
            } else {
                $result = new Zend_Auth_Result(Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID, $user, ['Password mismatch']);
            }
        } else {
            $result = new Zend_Auth_Result(Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND, $user, ['Username not found']);
        }
        return $result;
    }
    
    public function generateNewPassword($password) 
    {        
        $hash = password_hash($password, PASSWORD_BCRYPT, $this->hashPasswordOptions);
        return $hash;
    }
    
    public function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
        $this->_helper->redirector('index'); // back to login page
    }
}

