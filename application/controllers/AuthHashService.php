<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class AuthHashService implements Zend_Auth_Adapter_Interface {

protected $email;
protected $password;

public function __construct($email, $password) {
    $this->email = $email;
    $this->password = $password;
}

    public function authenticate() {

        $adapter = new Zend_Auth_Adapter_DbTable();
        $userTable = new Model_DbTable_User();
        
        $user = $userTable->fetchRow($userTable->select()->where(array('username = ?' => $username, 'company' => $params['companyname'])));

        //$user = new Model_DbTable_User($adapter->fetchRow('email', $this->email));
        //if you want to use the Zend_Auth_Result::FAILURE_IDENTITY_AMBIGUOUS error condition, use fetchAll
        if ($user->password) {
            if (password_verify($this->password, $user->password)) {
                //User OK
                return new Zend_Auth_Result(Zend_Auth_Result::SUCCESS, $user);
            } else {
                return new Zend_Auth_Result(Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID, $user, ['Password mismatch']);
            }
        } else {
            return new Zend_Auth_Result(Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND, $user, ['Email not found']);
        }
    }
    
    public function generateHash() {
        
    }
}
