<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    
    protected function _initDatabase()
    {
        $this->bootstrap('db');
        $resource = $this->getPluginResource('db');
        $db       = $resource->getDbAdapter();
        $dbConfig = $db->getConfig();
        Zend_Registry::set('dbConfig', $dbConfig);
    }
    
    protected function _initAutoLoad()
    {
        /** auto load */
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->setFallbackAutoloader(TRUE);
        $moduleLoad = new Zend_Application_Module_Autoloader(array(
            'namespace' => '',
            'basePath'  => APPLICATION_PATH
        ));

    }
    protected function _initJavascript()
    {
        $this->bootstrap('layout');
        $layout = $this->getResource('layout');
        $view   = $layout->getView();
        $view->headScript()->appendFile('/js/jquery-1.11.0.min.js', 'text/javascript');
        $view->headScript()->appendFile('/js/bootstrap.min.js', 'text/javascript');
        $view->headScript()->appendFile('/js/slidesbg.js', 'text/javascript');
        //$view->headScript()->appendFile('/js/bootstrap-modal.js', 'text/javascript');
        $view->headScript()->appendFile('/js/jquery.validate.min.js', 'text/javascript');
        // idle timeout
        //$view->headScript()->appendFile('/js/jquery.idletimer.js');
        //$view->headScript()->appendFile('/js/jquery.idletimeout.js');
    }

    protected function _initStyleSheets()
    {
        $this->bootstrap('layout');
        $layout = $this->getResource('layout');
        $view   = $layout->getView();
        $view->headLink()->appendStylesheet('/css/jquerysctipttop.css');
        $view->headLink()->appendStylesheet('/css/bootstrap.min.css');
        //$view->headLink()->appendStylesheet('/css/bootstrap-responsive.css');
        $view->headLink()->appendStylesheet('/css/slidesbg.css');
        
        $view->headLink(array('rel'  => 'shortcut icon',
                              'href' => '/images/favicon.ico',
                              'type' => 'image/ico'));
    }

}

