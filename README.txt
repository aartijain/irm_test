README
======

This directory should be used to place project specfic documentation including
but not limited to project notes, generated API/phpdoc documentation, or
manual files generated or hand written.  Ideally, this directory would remain
in your development environment only and should not be deployed with your
application to it's final production location.


Setting Up Your VHOST
=====================

The following is a sample VHOST you might want to consider for your project.

<VirtualHost *:80>
   DocumentRoot "C:/GITWORK/irm_test/public"
   ServerName irm_test.local

   # This should be omitted in the production environment
   SetEnv APPLICATION_ENV development

   <Directory "C:/GITWORK/irm_test/public">
       Options Indexes MultiViews FollowSymLinks
       AllowOverride All
       Order allow,deny
       Allow from all
       Require all granted
   </Directory>

</VirtualHost>

(1) Application credentials:
Companyname: test
Username: test
Password: password

# After login if you want to login with newly encrypted password please use below link.

http://irm.local/login/processlogin 